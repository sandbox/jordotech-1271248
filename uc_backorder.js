$(document).ready(
        function(){
            $('#uc-cim-terminal-form #edit-amount-wrapper input').val(0);
            $('#uc-cim-refund-form #edit-amount-wrapper input').val(0);

            $.partialTotal = {
                total : 0,
				//sometimes calculating tax for individual items can lead to mismatches in rounding leading to the charge amount being off by one penny.  pennyFix() will adjust if one cent off.
				pennyFix : function(){
					var chargeAmt = $('.partial-charge-amount').val().replace(',', '');
    				var balance = $('#uc-cim-terminal-form').prev().children().children().next().children().next().html();
					balance = balance.substring(1).replace(',', '');
					var difference = chargeAmt - balance;
					difference.toFixed(2);
					difference = Math.abs(difference).toFixed(2);
					if(difference == 0.01){
						$('.partial-charge-amount').val(balance);
					}
},
calcPartial : function(){
    var grandTotal = 0;
    $('#show_math').html('');
    $('#show_math').append('<table>');
    $('#uc-cim-terminal-form .form-select').each(function(){
        // Loop through all the select forms and calculate the total.
        var thisQuantity = $(this).val();
        var thisAmount = $(this).parent().next().html();
        var thisTitle = $(this).parent().next().next().html();
        var thisLineTotal = thisQuantity * thisAmount;
        $('#show_math').append('<tr><td> + </td><td>' + thisLineTotal + '</td><td>' + thisTitle + '<td></tr>');
        grandTotal = parseFloat(grandTotal) + parseFloat(thisLineTotal);
        grandTotal = grandTotal.toFixed(2);
        $('#uc-cim-terminal-form #edit-amount-wrapper input').val(grandTotal);
    });

    $('#show_math').append('</table>');
    $('#show_math').append('<hr/><div class="partial_total" style="margin-left:20px;font-weight:bold;">' + grandTotal + ' Total</div>');
	$.partialTotal.pennyFix();
},

calcRefund : function(){
    var grandTotal = 0;
    $('#show_math_refund').html('');
    $('#show_math_refund').append('<table>');
    $('#uc-cim-refund-form .form-select').each(function(){
        // Loop through all the select forms and calculate the total.
        var thisQuantity = $(this).val();
        var thisAmount = $(this).parent().next().html();
        var thisTitle = $(this).parent().next().next().html();
        var thisLineTotal = thisQuantity * thisAmount;
        $('#show_math_refund').append('<tr><td> + </td><td>' + thisLineTotal + '</td><td>' + thisTitle + '<td></tr>');
        grandTotal = parseFloat(grandTotal) + parseFloat(thisLineTotal);
        grandTotal = grandTotal.toFixed(2);
        $('#uc-cim-refund-form #edit-amount-wrapper input').val(grandTotal);
    });

    $('#show_math_refund').append('</table>');
    $('#show_math_refund').append('<hr/><div class="partial_total" style="margin-left:20px;font-weight:bold;">' + grandTotal + ' Total</div>');
	$.partialTotal.pennyFix();

},
}
$('#uc-cim-terminal-form .form-select').each(
    function(){
        $(this).change(function(){
            $.partialTotal.calcPartial();
        });
    }
    );

$('#uc-cim-refund-form .form-select').each(
    function(){
        $(this).change(function(){
            $.partialTotal.calcRefund();
        });
    }
    );
var partial = $.partialTotal.total;
$('.pretotal').html(partial);
$.partialTotal.calcPartial();
$.partialTotal.calcRefund();
$.partialTotal.pennyFix();

}
);
